<html>
	<head>
	  <title>Bootstrap Example</title>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body class="container">
		<form method="POST" class="text-center">
			<h1>Add Two Number </h1>
			 <div class="form-group">
				<label>x</label>
				<input type="text" name="x" autofocus value="<?php echo isset($_REQUEST['x']) ? $_REQUEST['x'] : ''; ?>"  />
			 </div>
			 <div class="form-group">
				<label>y</label>
				<input type="text" name="y" value="<?php echo isset($_REQUEST['y']) ? $_REQUEST['y'] : ''; ?>" />
			 </div>
			 
			<input class="btn btn-default" type="submit" value="+" name="submit">
			<input class="btn btn-default" type="submit" value="-" name="submit">
			<input class="btn btn-default" type="submit" value="*" name="submit">
			<input class="btn btn-default" type="submit" value="/" name="submit">
		</form>
		<div class="text-center">
		<?php
			
			if(isset($_REQUEST['x']) && isset($_REQUEST['y'])) {
			
				$x = $_REQUEST['x'];
				$y = $_REQUEST['y'];
				
				switch($_REQUEST['submit']) {
					case '+' :
						echo "<h1> $x + $y = " . ($x+$y) ." </h1>";
						break;
				
					case '-' :
						echo "<h1> $x - $y = " . ($x-$y) ." </h1>";
						break;
						
					case '*' :
						echo "<h1> $x * $y = " . ($x*$y) ." </h1>";
						break;

					case '/' :
						echo "<h1> $x / $y = " . ($x/$y) ." </h1>";
						break;
				}
			} else {
				echo "Output will come here";
			}
		?>
		</div>
	</body>
</html>