<?php 
include 'header1.php';
include 'db_conn.php';

$sql = "SELECT car.id, year(car.Manufactureryear),manufacturer.manufacturerName, car.carname,car.color,car.Quntity, car.cost FROM car LEFT JOIN manufacturer ON car.Manufacturername = manufacturer.id";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
	?>
<!DOCTYPE html>
<html>
<head>
        <script src="jquery/jquery.min.js"></script>
        <link href="css/jquery.dataTables.min.css" rel="stylesheet">
        <script src="js/jquery.dataTables.min.js"></script>   
	</head>
<body>
	<div class="container">
    <table id="example" class="display" style="width:100%">
	<thead>
		<tr>
		  <th>Id Number</th>
		  <th>Year</th>
		  <th>Manufacturer Name</th>
		  <th>Car name</th>
		  <th>Color</th>
		  <th>Quntity</th>
		  <th>Cost</th>	  
		  <th>Action</th>
		</tr>
	 </thead>
	
	<?php 
		if(isset($result)){
			foreach($result as $record){
	?>
		<tr>
			<td> <?php echo $record['id']; ?></td>
			<td> <?php echo $record['year(car.Manufactureryear)']; ?></td>
			<td> <?php echo $record['manufacturerName']; ?></td>
			<td> <?php echo $record['carname']; ?></td>
			<td> <?php echo $record['color']; ?></td>
			<td> <?php echo $record['Quntity']; ?></td>
			<td> <?php echo $record['cost']; ?></td>
			<td> <a href="admin_car_sold.php?id=<?php echo $record['id']; ?>"/><input type = "button" value = "SOLD" class="btn btn-success"></input></a></td>
		</tr>
	
	<?php
		}
	}
}
	//header("Location:show.php");
	?>
	<tfoot>
         <tr>
			<th>Id Number</th>
			<th>Year</th>
			<th>Manufacturer Name</th>
			<th>Car name</th>
			<th>Color</th>
			<th>Quntity</th>
			<th>Cost</th>	  
			<th>Action</th>
        </tr>
	</tfoot>
</body>
	<script>
			 $(document).ready( function () {
            $('#example').DataTable();
        });
		</script>
</html>