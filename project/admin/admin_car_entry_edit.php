<?php include 'header1.php';
include 'db_conn.php';

$sql = "SELECT car.*, manufacturer.manufacturerName AS name FROM car LEFT JOIN manufacturer ON car.Manufacturername = manufacturer.id ";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
	?>
<!DOCTYPE html>
<html>
	<head>
        <script src="jquery/jquery.min.js"></script>
        <link href="css/jquery.dataTables.min.css" rel="stylesheet">
        <script src="js/jquery.dataTables.min.js"></script>   
	</head>
	<body>   
	    <table id="example" class="display" style="width:100%">
            <thead>
                <tr>
                <th>Id</th>
				<th>Manufacturer name</th>
				<th>Manufacturer year</th>
				<th>Model name</th>
				<th>Color</th>
				<th>Quntity</th>
				<th>Cost</th>
				<th>Discription</th>
				<th>Action</th>
                </tr>
            </thead>
			<?php 
		if(isset($result)){
			foreach($result as $record){
	?>
		<tr>
			<td> <?php echo $record['id']; ?></td>
			<td> <?php echo $record['name']; ?></td>
			<td> <?php echo $record['Manufactureryear']; ?></td>
			<td> <?php echo $record['carname']; ?></td>
			<td> <?php echo $record['color']; ?></td>
			<td> <?php echo $record['Quntity']; ?></td>
			<td> <?php echo $record['cost']; ?></td>
			<td> <?php echo $record['disciption']; ?></td>
			<td> <a href="admin_car_stock.php?id=<?php echo $record['id']; ?>"><input type = "button" class="btn btn-info" value ="Edit"></input></a>&nbsp; 
			 <a href="admin_car_delete.php?id=<?php echo $record['id']; ?>"><input type = "button" class="btn btn-danger" value ="Delete"></input></a></input></td>
		</tr>
	
	<?php
		}
	}
}
	//header("Location:show.php");
	?>
			<tfoot>
                <tr>
                    <th>Id</th>
					<th>Manufacturer name</th>
					<th>Manufacturer year</th>
					<th>Model name</th>
					<th>Color</th>
					<th>Quntity</th>
					<th>Cost</th>
					<th>Discription</th>
					<th>Action</th>
				</tr>
            </tfoot>
		</table>
	</body>
		<script>
			 $(document).ready( function () {
            $('#example').DataTable();
        });
		</script>
</html>