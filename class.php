<?php
 class rectangle{
	 public $length = 1;
	 public $width = 1;
	 function area(){
		 $area = $this -> length * $this -> width;
		 echo "area of rectangle is = $area<br>";
	 }
	 function perimeter(){
		 $perimeter = 2 * ($this -> length + $this -> width);
		 echo "perimeter of rectangle is = $perimeter<br>";
	 }
	 function __construct($l=1,$w=1){
			$this->length=$l;
			$this->width=$w;
		}
 }
	$obj1 = new rectangle();
	$obj1 -> length = 10;
	$obj1 -> width = 5;
	$obj1 -> area();
	$obj1 -> perimeter();
	$obj2= new rectangle(5, 7);
	$obj2 -> area();
?>