<?php
	class Student {
		public $name;
		public $age;
		public $city;
		public $country;
		
		function __construct($n,$a,$c,$d){
			$this->name=$n;
			$this->age=$a;
			$this->city=$c;
			$this->country=$d;
		}
		function __toString(){
			return "name:".$this->name."<br>".
					"Age:".$this->age. "<br>".
					"City:".$this->city."<br>".
					"Country:".$this->country;
					
		}
	}
	
$std1= new Student("ravi",25,"mumbai","pakistan");
//var_dump($std1);
//var_dump($std1, (string) $std1);
echo $std1;
echo "</br>";
$std2 = New Student("raj",22,"nagpur","India");
echo $std2;
echo "<br>";
$json = json_encode($std2);
var_dump($json);
echo "<br>";
echo '<pre>' . print_r($json, true) . '</pre>';

$section = file_get_contents('append.txt');
var_dump($section);
$j = json_decode($section, true);
echo '<pre>' . print_r($j, true) . '</pre>';
//file_put_contents("append.txt", $json . "\n", FILE_APPEND);

?>
